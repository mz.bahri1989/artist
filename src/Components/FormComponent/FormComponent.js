import React, { Component } from 'react';
import {connect} from "react-redux";
import {store} from '../../Store';
import {FormChangeAction} from "../../Store/Actions/FormChangeAction";
import {SubmitFormAction} from "../../Store/Actions/SubmitFormAction";
import {ToggleLoadingAction} from "../../Store/Actions/ToggleLoadingAction";

class FormComponent extends Component{
    constructor(props){
        super(props);
        this.submitForm = this.submitForm.bind(this);
        this.updateName = this.updateName.bind(this);
    }

    updateName(event) {
        store.dispatch(FormChangeAction(event.target.value))
    }



    submitForm(event){
        event.preventDefault();
        console.log('form is submitted');
        store.dispatch(ToggleLoadingAction(this.props.loading));
        store.dispatch(SubmitFormAction(this.props.form.artist))
    }
    render(){
        const {artist, loading, noArtist, err} = this.props.form;
        return(
            <div className="wrapper">
                <form method="post" action="" onSubmit={this.submitForm}>
                    <input type="text" placeholder="Enter the name of the artist . . ."
                           value={artist}
                           onChange={this.updateName}
                    />
                    <input type="submit" value="submit" />
                </form>
                <img src="./loading.gif" className={'loading' + (loading ? 'show':'')} alt="loading icon"/>
                <p className={'err'+ (noArtist ? 'show':'')}>No such artist. Try again.</p>
                {err ? <p className="err">An error occured. Please Try again</p>:''}
            </div>

        )
    }
}
const mapStateToProps = state => {
    return{
        form: state.form
    }
};
export default connect(mapStateToProps,{})(FormComponent);
import React, {Component} from 'react';
import FormComponent from "../FormComponent/FormComponent";
import ArtistComponent from "../ArtistComponent/ArtistComponent";
import {connect} from "react-redux";


class App extends Component {
    render() {
        return (
            <div className="App">
                <h1>Artist Finder</h1>
                <p>Powered By <a href="https://bandsintown.com">bandsintown.com</a></p>
                <FormComponent/>
                {this.props.status ? <ArtistComponent/> : ''}

            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        status: state.form.status
    }
};


export default connect(mapStateToProps, {})(App);

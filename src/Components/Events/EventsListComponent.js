import React, {Component} from 'react';
import EventComponent from './EventComponent'
import {connect} from "react-redux";

class EventsListComponent extends Component {
    constructor(props){
        super(props);
        this.setUpContent = this.setUpContent.bind(this);
    }
    setUpContent() {
        const {eventList, requestDone} = this.props.events;
        if (!requestDone) {
            return ''
        }
        if (!Array.isArray(eventList) || eventList.length <= 0) {
            return <p>No Events For Now :(</p>
        }
        return <ol>
            {eventList.map((item) => {
                return <EventComponent date={item.datetime}
                                       name={item.venue.name}
                                       city={item.venue.city}
                                       country={item.venue.country}
                />
            })}
        </ol>
    }

    render() {
        return (
            <div className="card">
                <p>{this.props.name}'s Events</p>
                {this.setUpContent()}
            </div>

        )

    }
}

const mapStateToProps = state => {
    return {
        events: state.events,
        name: state.artistDetails.name
    }
};


export default connect(mapStateToProps, {})(EventsListComponent);
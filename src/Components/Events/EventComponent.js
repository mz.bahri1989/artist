import React, {Component} from 'react';

export default class EventComponent extends Component {
      render() {
        const {name, city, country, date} = this.props;
        return (
            <li>
                {name}
                <br/>
                {city}
                <br/>
                {country}
                <br/>
                {date}
            </li>
        )

    }
}

import {_SENDING} from "../types";

export function ToggleLoadingAction(payload){
    return {
        type:_SENDING,
        payload:!payload
    }
}
import {_EVENTS} from "../types";

export function GetEventsCompleteAction(payload){
    return {
        type: _EVENTS,
        payload
    }
}
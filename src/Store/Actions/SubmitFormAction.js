import {_APPID, _URL} from "../types";
import axios from 'axios';
import {SearchCompleteAction} from "./SearchCompleteAction";
import {FailureAction} from "./FailureAction";
import {NoArtistAction} from "./NoArtistAction";
export function SubmitFormAction(payload){
    return(dispatch=>{
        axios({
            method:'GET',
            url:_URL+payload+'?app_id='+_APPID,
        }).then((res)=>{
            if(res.data.error){
                return dispatch(NoArtistAction());

            }
            return dispatch(SearchCompleteAction(res.data))
        }).catch((err)=>{
            return dispatch(FailureAction(err))
        })
    })
}
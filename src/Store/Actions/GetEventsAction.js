import {_APPID, _URL} from "../types";
import axios from 'axios';
import {FailureAction} from "./FailureAction";
import {GetEventsCompleteAction} from "./GetEventsCompleteAction";
export function GetEventsAction(payload){
    return(dispatch=>{
        axios({
            method:'GET',
            url:_URL+payload+'/events?app_id='+_APPID,

        }).then((res)=>{
            console.log(res);
            return dispatch(GetEventsCompleteAction(res.data))
        }).catch((err)=>{
            console.log(err);
            return dispatch(FailureAction(err))
        })
    })
}
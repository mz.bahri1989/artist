import {_NEW_ARTIST} from "../types";

export function SearchCompleteAction(payload){
    return {
        type: _NEW_ARTIST,
        payload
    }
}
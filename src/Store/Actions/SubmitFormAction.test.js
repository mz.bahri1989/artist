import configureMockStore from 'redux-mock-store'
import * as action from './SubmitFormAction'
import {_ERR, _NEW_ARTIST} from "../types";
import thunk from 'redux-thunk'

const middlewares = [thunk];

const mockStore = configureMockStore(middlewares);
const store = mockStore({ artistDetails: {} });

describe('async actions', () => {
    it('Submit  the form', () => {
        const payload = 'Taylor Swift';

        const expectedActions = [{ type: _ERR } , { type: _NEW_ARTIST,
                payload: {
                facebook_page_url: "",
                id: "157",
                image_url: "https://s3.amazonaws.com/bit-photos/large/8652942.jpeg",
                mbid: "20244d07-534f-4eff-b4d4-930878889970",
                name: "Taylor Swift",
                thumb_url: "https://s3.amazonaws.com/bit-photos/thumb/8652942.jpeg",
                tracker_count: 5772435,
                upcoming_event_count: 0,
                url: "https://www.bandsintown.com/a/157?came_from=267&app_id=xckvsufodijp4098423094isfodfu8xhsrnfkjhvie"}
            }];

        // return of async actions
        return store.dispatch(action.SubmitFormAction(payload)).then(() => {
            // return of async actions
            expect(store.getActions()).toEqual(expectedActions)
        })
    })
});
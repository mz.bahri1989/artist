import {_NAME_CHANGED} from "../types";

export function FormChangeAction(payload){
    return{
        type:_NAME_CHANGED,
        payload
    }
}
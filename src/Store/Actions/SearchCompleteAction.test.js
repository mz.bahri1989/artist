import * as action from './SearchCompleteAction';
import {_NEW_ARTIST} from "../types";

describe('actions', () => {
    it('should create an action show a new artist', () => {
        const payload = {};
        const expectedAction = {
            type: _NEW_ARTIST,
            payload
        }
        expect(action.SearchCompleteAction(payload)).toEqual(expectedAction)
    })
})
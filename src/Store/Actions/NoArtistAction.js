import {_NO_ARTIST} from "../types";

export function NoArtistAction(){
    return {
        type: _NO_ARTIST
    }
}
import {_ERR} from "../types";

export function FailureAction(payload){
    return {
        type: _ERR,
        payload
    }
}
import {_NAME_CHANGED, _NEW_ARTIST} from "../types";

const INITIAL_STATE = {
    id:null,
    name:'',
    url:'',
image_url:'',
    thumb_url:'',
    facebook_page_url:'',
    mbid:'',
tracker_count:null,
upcoming_event_count:''
};

export function ArtistReducer(state = INITIAL_STATE,action){
    switch(action.type){
        case _NAME_CHANGED:
            return INITIAL_STATE;
        case _NEW_ARTIST:
            state = action.payload;
            return state;
        default:
            return state;

    }
}
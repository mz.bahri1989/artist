import {root} from './Reducers/index';
import {applyMiddleware, createStore} from 'redux';
import thunk from "redux-thunk";
export const store = createStore(root, applyMiddleware(thunk));